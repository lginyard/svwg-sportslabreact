import React from 'react';
import './App.css';
import Game from './Game'
import './style.css'
import Brooklynlogo from './Brooklynlogo.jpg'
import Queenslogo from './Queenslogo.jpg'
import Statenilogo from './Statenilogo.jpg'
import Bronxlogo from './Bronxlogo.jpg'


function App(props) {

  const Brooklyn = {
    name: "Brooklyn Knights",
    logo: Brooklynlogo,
  };
  const Queens = {
    name: "Queens Village",
    logo: Queenslogo,
  };

  const StatenI = {
    name: "Staten Island Lights",
    logo: Statenilogo,
  }

  const Bronx = {
    name: "Bronx Heights",
    logo: Bronxlogo,
  }
  return (
    <div className="App">
      <Game venue="State Street Dome" homeTeam={Brooklyn} 
      visitingTeam={Queens} />
    <div className="clear"></div>

    <Game venue="Madison Square Garden" homeTeam={StatenI}
    visitingTeam={Bronx} />
    </div>
  );
}

export default App;
